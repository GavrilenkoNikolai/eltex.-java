package lab1;

class Caps extends Clothes {
    private static int firstStart = 0;

    Caps(int arg) {
        getDelete(arg);
    }

    Caps(String arg) {
        if (firstStart == 0) {
            for (int i = 0; i < Integer.parseInt(arg); i++) {
                getCreate();
            }
            firstStart++;
            getRead();
        } else {
            getCreate(arg);
        }
    }

    Caps(int num, String arg) {
        getUpdate(num, arg);
    }
}
