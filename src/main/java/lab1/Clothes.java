package lab1;

import java.util.Random;
import java.util.UUID;

public abstract class Clothes implements ICrudAction {
    private static String[] mass;
    private int count = 0;
    String uniqueID;

    public void getCreate() {
        Random ran = new Random();
        int length = count + 1;

        if (length > 1) {
            String[] temp = new String[mass.length];
            for (int i = 0; i < mass.length; i++) {
                temp[i] = mass[i];
            }
            mass = new String[length];
            for (int i = 0; i < temp.length; i++) {
                mass[i] = temp[i];
            }
            mass[count] = "Название" + ran.nextInt(10) + " " + ran.nextInt(10000) + " " +
                    ran.nextInt(100) + " " + " Фирма-производитель" + ran.nextInt(10) + " ID: " + UUID.randomUUID().toString();
        } else {
            mass = new String[length];
            mass[count] = "Название" + ran.nextInt(10) + " " + ran.nextInt(10000) + " " +
                    ran.nextInt(100) + " " + " Фирма-производитель" + ran.nextInt(10) + " ID: " + UUID.randomUUID().toString();
        }
        count++;
    }

    public void getCreate(String arg) {
        int length = count + 1;

        String[] temp = new String[mass.length];
        System.arraycopy(mass, 0, temp, 0, mass.length);
        mass = new String[length];
        System.arraycopy(temp, 0, mass, 0, temp.length);
        mass[count] = arg + " ID: " + UUID.randomUUID().toString();
        count++;
        getRead();
    }

    public void getRead() {
        for (int i = 0; i < mass.length; i++) {
            System.out.println((i + 1) + ": " + mass[i]);
        }
    }

    public void getUpdate(int num, String arg) {
        String[] temp = new String[mass.length];
        System.arraycopy(mass, 0, temp, 0, mass.length);
        mass = new String[mass.length];
        for (int i = 0; i < temp.length; i++) {
            if (i != num - 1) {
                mass[i] = temp[i];
            } else {
                mass[i] = arg + " ID: " + UUID.randomUUID().toString();
            }
        }
        getRead();
    }

    public void getDelete(int arg) {
        int j = 0;

        String[] temp = new String[mass.length];
        System.arraycopy(mass, 0, temp, 0, mass.length);
        mass = new String[mass.length - 1];
        for (int i = 0; i < temp.length; i++) {
            if (i != arg - 1) {
                mass[j] = temp[i];
                j++;
            }
        }
        getRead();
    }

    public void getDelete() {

    }

    public void getUpdate() {

    }
}
