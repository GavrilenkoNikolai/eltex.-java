package lab1;

public interface ICrudAction {
    void getCreate();

    void getRead();

    void getUpdate();

    void getDelete();
}
