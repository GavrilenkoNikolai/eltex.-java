package lab1;

class Shirts extends Clothes {

    private static boolean firstStart = true;

    Shirts(int arg) {
        getDelete(arg);
    }

    Shirts(String arg) {
        if (firstStart) {
            for (int i = 0; i < Integer.parseInt(arg); i++) {
                getCreate();
            }
            getRead();
        } else {
            getCreate(arg);
        }
        firstStart = false;
    }

    Shirts(int num, String arg) {
        getUpdate(num, arg);
    }
}
