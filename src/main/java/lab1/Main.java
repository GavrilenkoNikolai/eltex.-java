package lab1;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner scanExit = new Scanner(System.in);
        String str = "";
        boolean bol = true;

        while (!str.equals("y")) {
            if (Integer.parseInt(args[1]) == 1) {
                if (bol) {
                    new Caps(args[0]);
                    bol = false;
                }
                System.out.println("Введите, через пробел: Название Цена Счётчик товаров Фирма-производитель, для добаления ");
                System.out.println("Введите, по шаблону: номер - Название Цена Счётчик товаров Фирма-производитель для редактирования");
                System.out.println("Введите, номер строки для удаления");
                Scanner scan = new Scanner(System.in);
                str = scan.nextLine();
                String[] subStr;
                String delimeter = " - "; // Разделитель
                subStr = str.split(delimeter); // Разделения строки str с помощью метода split()

                if (subStr.length == 1 && !isInteger(subStr[0])) {
                    new Caps(str);
                } else if (subStr.length == 1 && isInteger(subStr[0])) {
                    new Caps(Integer.parseInt(subStr[0]));
                } else if (subStr.length >= 2) {
                    new Caps(Integer.parseInt(subStr[0]), subStr[1]);
                } else {
                    System.out.println("Неправильный ввод ");
                }
            } else if (Integer.parseInt(args[1]) == 2) {
                if (bol) {
                    new Shirts(args[0]);
                    bol = false;
                }
                System.out.println("Введите, через пробел: Название Цена Счётчик товаров Фирма-производитель, для добаления ");
                System.out.println("Введите, по шаблону: номер - Название Цена Счётчик товаров Фирма-производитель для редактирования");
                System.out.println("Введите, номер строки для удаления");
                Scanner scan = new Scanner(System.in);
                str = scan.nextLine();
                String[] subStr;
                String delimeter = " - "; // Разделитель
                subStr = str.split(delimeter); // Разделения строки str с помощью метода split()

                if (subStr.length == 1 && !isInteger(subStr[0])) {
                    new Shirts(str);
                } else if (subStr.length == 1 && isInteger(subStr[0])) {
                    new Shirts(Integer.parseInt(subStr[0]));
                } else if (subStr.length >= 2) {
                    new Shirts(Integer.parseInt(subStr[0]), subStr[1]);
                } else {
                    System.out.println("Неправильный ввод ");
                }

            } else {
                System.out.println("Неправильные аргументы");
            }
            System.out.println("Вы хотите выйти[y/n]: ");
            str = scanExit.next();
        }
    }

    private static boolean isInteger(String s) {
        try {
            Integer.parseInt(s);
        } catch (NumberFormatException e) {
            return false;
        } catch (NullPointerException e) {
            return false;
        }
        return true;
    }
}
